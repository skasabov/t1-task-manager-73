package ru.t1.skasabov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tm_user")
public class UserDto extends AbstractModelDto {

    @NotNull
    @Column(nullable = false)
    private String login;

    @NotNull
    @Column(name = "passwordhash", nullable = false)
    private String passwordHash;

    public UserDto(@NotNull final String login, @NotNull final String passwordhash) {
        this.login = login;
        this.passwordHash = passwordhash;
    }

}
