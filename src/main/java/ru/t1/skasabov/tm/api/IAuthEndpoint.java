package ru.t1.skasabov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.Result;
import ru.t1.skasabov.tm.dto.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint {

    @NotNull
    @WebMethod
    Result login(
            @NotNull @WebParam(name = "login", partName = "login") String login,
            @NotNull @WebParam(name = "password", partName = "password") String password
    );

    @Nullable
    @WebMethod
    UserDto profile();

    @NotNull
    @WebMethod
    Result logout();

}
