package ru.t1.skasabov.tm.client.rest;

import feign.Feign;
import feign.okhttp.OkHttpClient;
import okhttp3.JavaNetCookieJar;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.t1.skasabov.tm.dto.Result;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.util.CookieManagerUtil;

import java.net.CookieManager;
import java.net.CookiePolicy;

public interface AuthRestEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/auth/";

    @NotNull
    static AuthRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        @NotNull final CookieManager cookieManager = CookieManagerUtil.cookieManager;
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        @NotNull final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));
        return Feign.builder()
                .client(new OkHttpClient(builder.build()))
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(AuthRestEndpointClient.class, BASE_URL);
    }

    @NotNull
    @PostMapping("/login")
    Result login(@NotNull @RequestParam("login") String login, @NotNull @RequestParam("password") String password);

    @Nullable
    @GetMapping("/profile")
    UserDto profile();

    @NotNull
    @PostMapping("/logout")
    Result logout();

}
