package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.api.IProjectEndpoint;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.skasabov.tm.api.IProjectEndpoint")
public final class ProjectEndpointImpl implements IProjectEndpoint {

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return projectRepository.findAllByUserId(UserUtil.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectDto save(
            @NotNull
            @RequestBody
            @WebParam(name = "project", partName = "project")
            final ProjectDto project
    ) {
        project.setUserId(UserUtil.getUserId());
        return projectRepository.save(project);
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @NotNull
            @PathVariable("id")
            @WebParam(name = "id", partName = "id")
            final String id
    ) {
        return projectRepository.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @PathVariable("id")
            @WebParam(name = "id", partName = "id")
            final String id
    ) {
        return projectRepository.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectRepository.countByUserId(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @PathVariable("id")
            @WebParam(name = "id", partName = "id")
            final String id
    ) {
        taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), id);
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @RequestBody
            @WebParam(name = "project", partName = "project")
            final ProjectDto project
    ) {
        taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
        projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project.getId());
    }

    @Override
    @WebMethod
    @PostMapping(value = "/deleteAll")
    public void deleteAll(
            @Nullable
            @RequestBody
            @WebParam(name = "projects", partName = "projects")
            final List<ProjectDto> projects
    ) {
        if (projects == null) return;
        for (@NotNull final ProjectDto project : projects) {
            taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
            projectRepository.deleteByUserIdAndId(UserUtil.getUserId(), project.getId());
        }
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        @Nullable final List<ProjectDto> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        if (projects == null) return;
        for (@NotNull final ProjectDto project : projects) taskRepository.deleteByUserIdAndProjectId(UserUtil.getUserId(), project.getId());
        projectRepository.deleteAllByUserId(UserUtil.getUserId());
    }

}
