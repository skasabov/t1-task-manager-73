package ru.t1.skasabov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

    @NotNull
    @Override
    protected SpringApplicationBuilder configure(@NotNull final SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

}
