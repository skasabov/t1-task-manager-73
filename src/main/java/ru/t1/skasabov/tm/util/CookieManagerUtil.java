package ru.t1.skasabov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.net.CookieManager;

public interface CookieManagerUtil {

    @NotNull
    CookieManager cookieManager = new CookieManager();

}
