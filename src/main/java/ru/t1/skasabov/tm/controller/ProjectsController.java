package ru.t1.skasabov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.skasabov.tm.dto.CustomUser;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;

import java.util.Collections;
import java.util.List;

@Controller
public final class ProjectsController {

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @NotNull
    @GetMapping("/projects")
    public ModelAndView index(@AuthenticationPrincipal @NotNull final CustomUser user) {
        @Nullable List<ProjectDto> projects = projectRepository.findAllByUserId(user.getUserId());
        if (projects == null) projects = Collections.emptyList();
        return new ModelAndView("project-list", "projects", projects);
    }

}
