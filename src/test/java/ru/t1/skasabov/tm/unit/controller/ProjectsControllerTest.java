package ru.t1.skasabov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.t1.skasabov.tm.dto.ProjectDto;
import ru.t1.skasabov.tm.marker.UnitCategory;
import ru.t1.skasabov.tm.repository.ProjectDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectsControllerTest {

    @NotNull
    private final ProjectDto project1 = new ProjectDto("Test Project 1");

    @NotNull
    private final ProjectDto project2 = new ProjectDto("Test Project 2");

    private boolean reqAuth = false;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @Before
    @SneakyThrows
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        authenticate();
        project1.setUserId(UserUtil.getUserId());
        project2.setUserId(UserUtil.getUserId());
        projectRepository.save(project1);
        projectRepository.save(project2);
    }

    @After
    public void clean() {
        if (reqAuth) {
            authenticate();
        }
        reqAuth = false;
        projectRepository.deleteAll();
        SecurityContextHolder.getContext().setAuthentication(null);
    }

    private void authenticate() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
        @Nullable final List<ProjectDto> projects = projectRepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @SneakyThrows
    @Category(UnitCategory.class)
    @Test(expected = NestedServletException.class)
    public void findAllNoAuthTest() {
        reqAuth = true;
        SecurityContextHolder.getContext().setAuthentication(null);
        @NotNull final String url = "/projects";
        mockMvc.perform(MockMvcRequestBuilders.get(url));
    }

}
