package ru.t1.skasabov.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.marker.UnitCategory;
import ru.t1.skasabov.tm.repository.UserDtoRepository;

@SpringBootTest
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDtoRepositoryTest {

    @NotNull
    @Autowired
    private UserDtoRepository userRepository;

    @Test
    @Category(UnitCategory.class)
    public void findByLoginTest() {
        @Nullable final UserDto user = userRepository.findByLogin("test");
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByEmptyLoginTest() {
        Assert.assertNull(userRepository.findByLogin(""));
    }

    @Test
    @Category(UnitCategory.class)
    public void findByInvalidLoginTest() {
        Assert.assertNull(userRepository.findByLogin("unknown"));
    }

}
